import 'package:flutter/material.dart';
import 'package:jointdoctor_app/src/model/DB.dart';
import 'file:///C:/Users/DARD_/OneDrive/Documentos/Uniandes/12/Moviles/flutter/lib/src/view/Contacts/ContactView.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'file:///C:/Users/DARD_/OneDrive/Documentos/Uniandes/12/Moviles/flutter/lib/src/view/Excersises/VideoExerciseList.dart';
import 'file:///C:/Users/DARD_/OneDrive/Documentos/Uniandes/12/Moviles/flutter/lib/src/view/Calendar/Calendar.dart';
import 'file:///C:/Users/DARD_/OneDrive/Documentos/Uniandes/12/Moviles/flutter/lib/src/view/Login/LoginScreen.dart';
import 'file:///C:/Users/DARD_/OneDrive/Documentos/Uniandes/12/Moviles/flutter/lib/src/view/Menu/MainMenu.dart';
import 'package:jointdoctor_app/src/controller/LogInState.dart';
import 'file:///C:/Users/DARD_/OneDrive/Documentos/Uniandes/12/Moviles/flutter/lib/src/view/Account/Account.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final FirebaseAnalytics _analytics = FirebaseAnalytics();

  FirebaseAnalyticsObserver getAnalyticsObserver() =>
      FirebaseAnalyticsObserver(analytics: _analytics);

  Future<void> appOpen() async {
    await _analytics.logAppOpen();
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<LogInState>(
          create: (context) => LogInState(),
        ),
        ChangeNotifierProvider<DB>(
          create: (context) => DB(),
        )
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
            // This is the theme of your application.
            //
            // Try running your application with "flutter run". You'll see the
            // application has a blue toolbar. Then, without quitting the app, try
            // changing the primarySwatch below to Colors.green and then invoke
            // "hot reload" (press "r" in the console where you ran "flutter run",
            // or simply save your changes to "hot reload" in a Flutter IDE).
            // Notice that the counter didn't reset back to zero; the application
            // is not restarted.
            primarySwatch: Colors.orange,
            scaffoldBackgroundColor: Colors.white),
        navigatorObservers: [getAnalyticsObserver()],
        routes: <String, WidgetBuilder>{
          '/': (BuildContext context) {
            return Consumer<LogInState>(builder: (context, state, child) {
              if (state.isLoggedIn()) {
                return MainMenu();
              } else {
                appOpen();
                return LoginScreen();
              }
            });
          },
          '/home': (BuildContext context) => MainMenu(),
          '/calendar': (BuildContext context) => Calendar(),
          '/account': (BuildContext context) => Account(),
          '/exercises': (BuildContext context) => VideoExerciseList(),
          '/contacts': (BuildContext context) => ContactView(),
        },
      ),
    );
  }
}
