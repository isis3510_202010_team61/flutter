import 'dart:typed_data';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class DB extends ChangeNotifier {
  String colUsers = "users";
  String colUserInfo = "userInfo";
  String colEmergencyContacts = "emergencyContacts";
  String colCalendarEvents = "calendarEvents";

  // User

  Future<void> addUser(String uid, String name, String email) {
    FirebaseFirestore.instance
        .collection(colUsers)
        .doc(uid)
        .collection(colUserInfo)
        .doc(uid)
        .set({"name": name, "email": email});
  }

  Future<void> getUserInfo(String uid) {
    FirebaseFirestore.instance
        .collection(colUsers)
        .doc(uid)
        .collection(colUserInfo)
        .doc(uid)
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        print(documentSnapshot.data()["email"]);
      } else {
        print("El documento no existe");
      }
    });
  }

  // Emergency Contacts

  Future<void> addEmergencyContact(
      String uid, String name, String phoneNumber) {
    FirebaseFirestore.instance
        .collection(colUsers)
        .doc(uid)
        .collection(colEmergencyContacts)
        .doc()
        .set(
      {"contactName": name, "contactPhone": phoneNumber},
    );
  }

  Future<QuerySnapshot> getEmergencyContacts(String uid) async {
    return await FirebaseFirestore.instance
        .collection(colUsers)
        .doc(uid)
        .collection(colEmergencyContacts)
        .get();
  }

  Future<void> deleteEmeregencyContact(String uid, String id) {
    FirebaseFirestore.instance
        .collection(colUsers)
        .doc(uid)
        .collection(colEmergencyContacts)
        .doc(id)
        .delete();
  }

  // Calendar Event

  Future<void> addCalendarEvent(
      String uid, String eventName, DateTime from, DateTime to, bool isAllDay) {
    FirebaseFirestore.instance
        .collection(colUsers)
        .doc(uid)
        .collection(colCalendarEvents)
        .doc()
        .set(
      {"eventName": eventName, "from": from, "to": to, "isAllDay": isAllDay},
    );
  }

  Future<QuerySnapshot> getCalendarEvents(String uid) async {
    return await FirebaseFirestore.instance
        .collection(colUsers)
        .doc(uid)
        .collection(colCalendarEvents)
        .get();
  }
}
