import 'package:flutter/material.dart';
import 'package:jointdoctor_app/src/controller/LogInState.dart';
import 'package:jointdoctor_app/src/model/DB.dart';
import 'file:///C:/Users/DARD_/OneDrive/Documentos/Uniandes/12/Moviles/flutter/lib/src/view/NavigationBar/NavigationBar.dart';
import 'package:provider/provider.dart';
import 'file:///C:/Users/DARD_/OneDrive/Documentos/Uniandes/12/Moviles/flutter/lib/src/view/NavigationBar/NavigationBar.dart';

class Account extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var user = Provider.of<LogInState>(context, listen: false).currentUser();
    var userInfo =
        Provider.of<DB>(context, listen: false).getUserInfo(user.uid);
    print(userInfo);
    var userName = user.displayName;
    var userEmail = user.email;
    var userImage = user.photoURL;
    return Scaffold(
      bottomNavigationBar: NavBar(
        index: 1,
      ),
      appBar: AppBar(
        title: const Text("Account", style: TextStyle(color: Colors.white)),
        centerTitle: true,
      ),
      body: Center(
          child: Container(
        padding: const EdgeInsets.all(32),
        child: Column(children: [
          Container(
            width: 150,
            height: 150,
            padding: const EdgeInsets.only(bottom: 10, top: 10),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(width: 2, color: Colors.orange),
              image: DecorationImage(
                  image: NetworkImage(userImage), fit: BoxFit.fill),
            ),
          ),
          Container(
              padding: const EdgeInsets.all(32),
              child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Name: ",
                        style: TextStyle(
                            color: Colors.orange, fontWeight: FontWeight.bold),
                      ),
                      Text(userName)
                    ],
                  ),
                ],
              )),
          Container(
              padding: const EdgeInsets.only(left: 32, top: 8),
              child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Email: ",
                        style: TextStyle(
                          color: Colors.orange,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(userEmail)
                    ],
                  ),
                ],
              ))
        ], crossAxisAlignment: CrossAxisAlignment.center),
      )),
    );
  }
}
