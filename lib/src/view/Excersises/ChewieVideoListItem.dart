import 'package:chewie/chewie.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter/material.dart';

class ChewieVideoListItem extends StatefulWidget {
  //This will contain the asset path which we want to play
  final VideoPlayerController videoPlayerController;
  final bool looping;

  ChewieVideoListItem({
    @required this.videoPlayerController,
    this.looping,
    Key key,
  }): super(key:key);

  @override
  _ChewieVideoListItem createState() => _ChewieVideoListItem();

}

class _ChewieVideoListItem extends State<ChewieVideoListItem> {
  ChewieController _chewieController;

  @override
  void initState() {
    super.initState();
    //Wrapper on top of the videoplayer controller
    _chewieController = ChewieController(
      videoPlayerController: widget.videoPlayerController,
      aspectRatio: 16/9,
      //Prepare the video to be played and display
      autoInitialize: true,
      looping: widget.looping,
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(padding: const EdgeInsets.all(8.0),
    child: Chewie(
      controller: _chewieController,
      ),
    );
  }


  @override
  void dispose() {
    super.dispose();
    //Important to dispose of all used reources
    widget.videoPlayerController.dispose();
    _chewieController.dispose();
  }
}