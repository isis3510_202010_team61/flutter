import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter/material.dart';
import 'file:///C:/Users/DARD_/OneDrive/Documentos/Uniandes/12/Moviles/flutter/lib/src/view/Excersises/ChewieVideoListItem.dart';
import 'package:firebase_storage/firebase_storage.dart';

class VideoExerciseList extends StatefulWidget {
  @override
  _VideoExcerciseListState createState() => _VideoExcerciseListState();
}

class _VideoExcerciseListState extends State<VideoExerciseList> {
  File videoFileOne;
  File videoFileTwo;

  @override
  void initState() {
    super.initState();
    downloadVideoOne();
    downloadVideoTwo();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _showDialog(title, text) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(title),
            content: Text(text),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  _checkConnectivity() {
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        return Scaffold(
            appBar: AppBar(
              title: Text("Contacts", style: TextStyle(color: Colors.white)),
            ),
            body: Center(
                child: _showDialog("No internet connection",
                    "You are not connected to a network")));
      }
    });
  }

  Future<File> downloadVideoOne() async {
    Directory videoDir = await getTemporaryDirectory();
    //print(videoDir.path);
    File vidFile = File("${videoDir.path}/videoExerciseOne.mp4");

    try {
      await FirebaseStorage.instance
          .ref()
          .child("videoExerciseOne.mp4")
          .writeToFile(vidFile);
    } on FirebaseException catch (e) {
      print(e);
    }

    return videoFileOne = vidFile;
  }

  Future<File> downloadVideoTwo() async {
    Directory videoDir = await getTemporaryDirectory();
    //print(videoDir.path);
    File vidFile = File("${videoDir.path}/videoExercise2.mp4");

    try {
      await FirebaseStorage.instance
          .ref()
          .child("videoExercise2.mp4")
          .writeToFile(vidFile);
    } on FirebaseException catch (e) {
      print(e);
    }

    return videoFileTwo = vidFile;
  }

  @override
  Widget build(BuildContext context) {
    _checkConnectivity();
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Excersices Video',
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
      ),
      body: ListView(
        children: <Widget>[
          FutureBuilder(
              future: downloadVideoOne(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  return Column(
                    children: [
                      Text("Excersice 1"),
                      ChewieVideoListItem(
                          videoPlayerController: VideoPlayerController.file(
                              File.fromUri(videoFileOne.uri)))
                    ],
                  );
                } else {
                  return CircularProgressIndicator();
                }
              }),
          FutureBuilder(
              future: downloadVideoTwo(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  return Column(
                    children: [
                      Text("Excersice 2"),
                      ChewieVideoListItem(
                          videoPlayerController: VideoPlayerController.file(
                              File.fromUri(videoFileTwo.uri)))
                    ],
                  );
                } else {
                  return CircularProgressIndicator();
                }
              })
        ],
      ),
    );
  }
}
