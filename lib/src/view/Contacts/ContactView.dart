import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:jointdoctor_app/src/controller/LogInState.dart';
import 'package:jointdoctor_app/src/model/DB.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:connectivity/connectivity.dart';
import 'package:provider/provider.dart';

import 'Contacts.dart';

class ContactView extends StatefulWidget {
  @override
  _ContactViewState createState() => _ContactViewState();
}

class _ContactViewState extends State<ContactView> {
  List contacts = [];
  List contactsId = [];
  Map<String, Color> contactsColorMap = new Map();
  FirebaseAnalytics analytics = FirebaseAnalytics();
  bool loading = false;

  @override
  void initState() {
    super.initState();
    getPermissions();
  }

  @override
  void dispose() {
    super.dispose();
  }

  getPermissions() async {
    if (await Permission.contacts.request().isGranted) {
      getEmergencyContacts();
    }
  }

  Future<void> getEmergencyContacts() async {
    var user = Provider.of<LogInState>(context, listen: false).currentUser();
    List colors = [Colors.green, Colors.indigo, Colors.yellow, Colors.orange];
    int colorIndex = 0;
    List eContacts = [];
    List eContactsId = [];
    var _contacts = await Provider.of<DB>(context, listen: false)
        .getEmergencyContacts(user.uid);
    _contacts.docs.forEach((contact) {
      eContacts.add(contact.data());
      eContactsId.add(contact.id);
      Color baseColor = colors[colorIndex];
      contactsColorMap[contact["contactName"]] = baseColor;
      colorIndex++;
      if (colorIndex == colors.length) {
        colorIndex = 0;
      }
    });
    _contacts.docs.removeWhere((element) => element["contactName"] == null);
    setState(() {
      contacts = eContacts;
      contactsId = eContactsId;
      loading = true;
    });
  }

  void deleteContact(String id) {
    var user = Provider.of<LogInState>(context, listen: false).currentUser();
    Provider.of<DB>(context, listen: false)
        .deleteEmeregencyContact(user.uid, id);
  }

  _showDialog(title, text) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(title),
            content: Text(text),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  _checkConnectivity() {
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      print(result);
      if (result == ConnectivityResult.none) {
        return Scaffold(
            appBar: AppBar(
              title: Text("Contacts", style: TextStyle(color: Colors.white)),
            ),
            body: Center(
                child: _showDialog("No internet connection",
                    "You are not connected to a network")));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    _checkConnectivity();
    return Scaffold(
        appBar: AppBar(
          title:
              Text("Emergency Contacts", style: TextStyle(color: Colors.white)),
          centerTitle: true,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            if (contacts.length >= 5) {
              _showDialog("Max number of Emergency Contacts",
                  "Delete one emergency contact to be able to add a new one");
            } else {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Contacts(),
                      settings: RouteSettings(name: "ContactsView")));
            }
          },
          child: Icon(Icons.add),
          backgroundColor: Colors.orange,
        ),
        body: Container(
            padding: EdgeInsets.all(20),
            child: Builder(
              builder: (context) {
                if (contacts.isEmpty && loading) {
                  return Text(
                      "No hay contactos de emergencia, agrega uno nuevo dandole al boton de +");
                } else if (loading) {
                  return RefreshIndicator(
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: contacts.length,
                            itemBuilder: (context, index) {
                              var baseColor = contactsColorMap[contacts[index]
                                  ["contactName"]] as dynamic;

                              Color color1 = baseColor[800];
                              Color color2 = baseColor[400];
                              return ListTile(
                                title: Text(contacts[index]["contactName"]),
                                subtitle: Text(contacts[index]["contactPhone"]),
                                leading: Container(
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        gradient: LinearGradient(
                                            colors: [
                                              color1,
                                              color2,
                                            ],
                                            begin: Alignment.bottomLeft,
                                            end: Alignment.topRight)),
                                    child: CircleAvatar(
                                        child: Icon(Icons.perm_identity),
                                        backgroundColor: Colors.transparent)),
                                trailing: IconButton(
                                  highlightColor: Colors.orange,
                                  onPressed: () {
                                    deleteContact(contactsId[index]);
                                  },
                                  icon: Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ),
                                ),
                              );
                            },
                          ),
                        )
                      ],
                    ),
                    onRefresh: getEmergencyContacts,
                  );
                } else {
                  return Align(
                      alignment: Alignment.bottomCenter,
                      child: Center(
                        child: CircularProgressIndicator(),
                      ));
                }
              },
            )));
  }
}
