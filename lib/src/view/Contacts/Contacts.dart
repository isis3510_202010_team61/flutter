import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:jointdoctor_app/src/controller/LogInState.dart';
import 'package:jointdoctor_app/src/model/DB.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:connectivity/connectivity.dart';
import 'package:provider/provider.dart';

class Contacts extends StatefulWidget {
  @override
  _ContactsState createState() => _ContactsState();
}

class _ContactsState extends State<Contacts> {
  List<Contact> contacts = [];
  List<Contact> contactsFiltered = [];
  Map<String, Color> contactsColorMap = new Map();
  TextEditingController searchController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    getPermissions();
    _checkConnectivity();
  }

  getPermissions() async {
    if (await Permission.contacts.request().isGranted) {
      getAllContacts();
      searchController.addListener(() {
        filterContacts();
      });
    }
  }

  _showDialog(title, text) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(title),
            content: Text(text),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  String flattenPhoneNumber(String phoneStr) {
    return phoneStr.replaceAllMapped(RegExp(r'^(\+)|\D'), (Match m) {
      return m[0] == "+" ? "+" : "";
    });
  }

  getAllContacts() async {
    List colors = [Colors.green, Colors.indigo, Colors.yellow, Colors.orange];
    int colorIndex = 0;
    List<Contact> _contacts = (await ContactsService.getContacts()).toList();
    _contacts.forEach((contact) {
      Color baseColor = colors[colorIndex];
      contactsColorMap[contact.displayName] = baseColor;
      colorIndex++;
      if (colorIndex == colors.length) {
        colorIndex = 0;
      }
    });
    _contacts.removeWhere((element) => element.displayName == null);
    setState(() {
      contacts = _contacts;
    });
  }

  filterContacts() {
    List<Contact> _contacts = [];
    _contacts.addAll(contacts);
    if (searchController.text.isNotEmpty) {
      _contacts.retainWhere((contact) {
        String searchTerm = searchController.text.toLowerCase();
        String searchTermFlatten = flattenPhoneNumber(searchTerm);
        String contactName = contact.displayName.toLowerCase();
        bool nameMatches = contactName.contains(searchTerm);
        if (nameMatches == true) {
          return true;
        }

        if (searchTermFlatten.isEmpty) {
          return false;
        }

        var phone = contact.phones.firstWhere((phn) {
          String phnFlattened = flattenPhoneNumber(phn.value);
          return phnFlattened.contains(searchTermFlatten);
        }, orElse: () => null);

        return phone != null;
      });
    }
    setState(() {
      contactsFiltered = _contacts;
    });
  }

  _checkConnectivity() {
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        return Scaffold(
            appBar: AppBar(
              title: Text("Contacts", style: TextStyle(color: Colors.white)),
            ),
            body: Center(
                child: _showDialog("No internet connection",
                    "You are not connected to a network")));
      }

/*      else if(result == ConnectivityResult.mobile){
        return Scaffold(
            appBar: AppBar(
              title: Text("Contacts",
                  style: TextStyle(color: Colors.white)),
            ),
            body: Center(
                child:  _showDialog("Internet Access", "You are connected over mobile data")
            )
        );
      }
      else if(result == ConnectivityResult.wifi){
        return Scaffold(
            appBar: AppBar(
              title: Text("Contacts",
                  style: TextStyle(color: Colors.white)),
            ),
            body: Center(
                child:  _showDialog("Internet Access", "You are connected over wifi")
            )
        );
      }*/
    });
  }

  @override
  Widget build(BuildContext context) {
    bool isSearching = searchController.text.isNotEmpty;
    return Scaffold(
        appBar: AppBar(
          title: Text("Add Contacts", style: TextStyle(color: Colors.white)),
          centerTitle: true,
        ),
        body: Builder(builder: (context) {
          if (contacts.isEmpty) {
            return Align(
              alignment: Alignment.bottomCenter,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          } else {
            return Container(
              padding: EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  Container(
                    child: TextField(
                      controller: searchController,
                      decoration: InputDecoration(
                          labelText: 'Search',
                          border: new OutlineInputBorder(
                              borderSide: new BorderSide(
                                  color: Theme.of(context).primaryColor)),
                          prefixIcon: Icon(Icons.search,
                              color: Theme.of(context).primaryColor)),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: isSearching == true
                          ? contactsFiltered.length
                          : contacts.length,
                      itemBuilder: (context, index) {
                        Contact contact = isSearching == true
                            ? contactsFiltered[index]
                            : contacts[index];
                        var baseColor =
                            contactsColorMap[contact.displayName] as dynamic;

                        Color color1 = baseColor[800];
                        Color color2 = baseColor[400];
                        return ListTile(
                            onTap: () {
                              var user = Provider.of<LogInState>(context,
                                      listen: false)
                                  .currentUser();
                              Provider.of<DB>(context, listen: false)
                                  .addEmergencyContact(
                                      user.uid,
                                      contact.displayName,
                                      contact.phones.elementAt(0).value);
                              Navigator.pop(context);
                            },
                            title: Text(contact.displayName),
                            subtitle: Text(contact.phones.length > 0
                                ? contact.phones.elementAt(0).value
                                : ''),
                            leading: (contact.avatar != null &&
                                    contact.avatar.length > 0)
                                ? CircleAvatar(
                                    backgroundImage:
                                        MemoryImage(contact.avatar),
                                  )
                                : Container(
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        gradient: LinearGradient(
                                            colors: [
                                              color1,
                                              color2,
                                            ],
                                            begin: Alignment.bottomLeft,
                                            end: Alignment.topRight)),
                                    child: CircleAvatar(
                                        child: Text(contact.initials(),
                                            style:
                                                TextStyle(color: Colors.white)),
                                        backgroundColor: Colors.transparent)));
                      },
                    ),
                  )
                ],
              ),
            );
          }
        }));
  }
}
