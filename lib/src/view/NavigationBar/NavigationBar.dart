import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class NavBar extends StatefulWidget {
  NavBar({
    @required this.index,
  });

  final int index;

  @override
  State<StatefulWidget> createState() => NavBarState();
}

class NavBarState extends State<NavBar> {
  @override
  void initState() {
    super.initState();
    _selectedIndex = widget.index;
    _getRoutes();
  }

  static int _selectedIndex;
  List<String> routes = new List<String>();

  _getRoutes() {
    routes.add('/'); //0
    // routes.add('/market');
    routes.add('/account'); //1
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: _selectedIndex, // this will be set when a new tab is tapped
      items: [
        BottomNavigationBarItem(
          icon: new Icon(Icons.home),
          title: new Text('Home'),
        ),
        // BottomNavigationBarItem(
        //   icon: new Icon(Icons.shopping_cart),
        //   title: new Text('Market'),
        // ),
        BottomNavigationBarItem(
          icon: Icon(Icons.account_circle),
          title: new Text('Account'),
        ),
      ],
      onTap: _onItemTapped,
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    Navigator.pushNamedAndRemoveUntil(
        context, routes[index], (Route<dynamic> route) => false);
  }
}
