import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:jointdoctor_app/src/controller/LogInState.dart';
import 'package:jointdoctor_app/src/model/DB.dart';
import 'package:provider/provider.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';

class LoginScreen extends StatelessWidget {
  static const String _title = 'Joint Doctor App';
  static FirebaseAnalytics analytics = FirebaseAnalytics();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text(
            _title,
            style: TextStyle(color: Colors.white),
          ),
          centerTitle: true),
      body: Scaffold(
        body: Consumer<LogInState>(
            builder: (BuildContext context, LogInState value, Widget child) {
          if (value.isLoading()) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return child;
          }
        }, child: MyStatelessWidget(onLoginSuccess: () {
          logIn(context);
        })),
      ),
    );
  }

  void logIn(BuildContext context) async {
    await analytics.logLogin(loginMethod: "google");
    Provider.of<LogInState>(context, listen: false).logIn();
    var user = Provider.of<LogInState>(context, listen: false).currentUser();
    await Provider.of<DB>(context, listen: false)
        .addUser(user.uid, user.displayName, user.email);
  }
}

class MyStatelessWidget extends StatelessWidget {
  final Function onLoginSuccess;
  MyStatelessWidget({Key key, this.onLoginSuccess}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const SizedBox(height: 30),
          ButtonTheme(
              minWidth: 200.0,
              height: 100.0,
              child: RaisedButton(
                onPressed: () {},
                color: Colors.orange,
                textColor: Colors.white,
                child: const Text('Sign In', style: TextStyle(fontSize: 30)),
              )),
          const SizedBox(height: 30),
          ButtonTheme(
              child: GoogleSignInButton(
            onPressed: () {
              onLoginSuccess();
            },
            darkMode: true,
            splashColor: Colors.orange,
            borderRadius: 50,
            textStyle: TextStyle(
                fontSize: 30, color: Colors.white, fontFamily: "Roboto"),
          )),
          const SizedBox(height: 70),
          ButtonTheme(
              minWidth: 200.0,
              height: 100.0,
              child: TextButton(
                onPressed: () {},
                child: const Text('Create an Account',
                    style: TextStyle(
                        fontSize: 30, decoration: TextDecoration.underline)),
              )),
        ],
      ),
    );
  }
}
