import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:speech_recognition/speech_recognition.dart';
import 'package:jointdoctor_app/src/controller/LogInState.dart';
import 'file:///C:/Users/DARD_/OneDrive/Documentos/Uniandes/12/Moviles/flutter/lib/src/view/Calendar/Calendar.dart';
import 'file:///C:/Users/DARD_/OneDrive/Documentos/Uniandes/12/Moviles/flutter/lib/src/view/NavigationBar/NavigationBar.dart';
import 'file:///C:/Users/DARD_/OneDrive/Documentos/Uniandes/12/Moviles/flutter/lib/src/view/Excersises/VideoExerciseList.dart';
import '../Contacts/ContactView.dart';

class MainMenu extends StatefulWidget {
  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {
  static const String _title = 'Main Menu';

  static FirebaseAnalytics analytics = FirebaseAnalytics();

  SpeechRecognition _speech;

  bool _speechRecognitionAvailable = false;
  bool _isListening = false;

  String transcription = '';

  @override
  initState() {
    super.initState();
    activateSpeechRecognizer();
  }

  @override
  dispose() {
    super.dispose();
    logOpenContactEvent();
  }

  void activateSpeechRecognizer() async {
    if (await Permission.microphone.request().isGranted) {
      print('_MyAppState.activateSpeechRecognizer... ');
      _speech = new SpeechRecognition();
      _speech.setAvailabilityHandler(onSpeechAvailability);
      _speech.setRecognitionStartedHandler(onRecognitionStarted);
      _speech.setRecognitionResultHandler(onRecognitionResult);
      _speech.setRecognitionCompleteHandler((text) {
        onRecognitionComplete();
      });
      _speech
          .activate()
          .then((res) => setState(() => _speechRecognitionAvailable = res));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          _title,
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        actions: [
          new IconButton(
              icon: Icon(Icons.logout, color: Colors.white, size: 30),
              onPressed: () =>
                  Provider.of<LogInState>(context, listen: false).logOut())
        ],
      ),
      body: MainMenuStructure(
        listening: _speechRecognitionAvailable && !_isListening
            ? () => start()
            : () => stop(),
        contactAnalytics: logOpenContactEvent(),
        calendarAnalytics: logOpenCalendarEvent(),
        excersicesAnalytics: logOpenExcersisesEvent(),
        voiceAnalytics: logVoiceEvent(),
      ),
      bottomNavigationBar: NavBar(index: 0),
    );
  }

  void start() => _speech
      .listen(locale: "es_ES")
      .then((result) => print('_MyAppState.start => result $result'));

  void cancel() =>
      _speech.cancel().then((result) => setState(() => _isListening = result));

  void stop() => _speech.stop().then((result) {
        setState(() => _isListening = result);
      });

  void onSpeechAvailability(bool result) =>
      setState(() => _speechRecognitionAvailable = result);

  void onRecognitionStarted() => setState(() => _isListening = true);

  void onRecognitionResult(String text) => setState(() => transcription = text);

  void onRecognitionComplete() {
    if (transcription.toLowerCase().contains('contact') ||
        transcription.toLowerCase().contains('contacts')) {
      Navigator.pushNamed(context, '/contacts');
    } else if (transcription.toLowerCase().contains('calendar')) {
      Navigator.pushNamed(context, '/calendar');
    } else if (transcription.toLowerCase().contains('exercises')) {
      Navigator.pushNamed(context, '/exercises');
    }
    setState(() {
      _isListening = false;
    });
  }

  Future<void> logOpenContactEvent() async {
    await analytics.logEvent(name: "click_on_contacts");
  }

  Future<void> logOpenCalendarEvent() async {
    await analytics.logEvent(name: "click_on_calendar");
  }

  Future<void> logOpenExcersisesEvent() async {
    await analytics.logEvent(name: "click_on_excersises");
  }

  Future<void> logVoiceEvent() async {
    await analytics.logEvent(name: "click_on_voice_comand");
  }

  void errorHandler() => activateSpeechRecognizer();
}

class MainMenuStructure extends StatelessWidget {
  MainMenuStructure(
      {Key key,
      this.listening,
      this.contactAnalytics,
      this.calendarAnalytics,
      this.excersicesAnalytics,
      this.voiceAnalytics})
      : super(key: key);
  final Function listening;
  final Future<void> contactAnalytics;
  final Future<void> calendarAnalytics;
  final Future<void> excersicesAnalytics;
  final Future<void> voiceAnalytics;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ButtonTheme(
              minWidth: 200.0,
              height: 100.0,
              child: RaisedButton(
                onPressed: () async {
                  //Navigator.push(context, MaterialPageRoute(builder: (context) => Calendar()));
                  await contactAnalytics;
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ContactView(),
                          settings:
                              RouteSettings(name: "EmergencyContactsView")));
                },
                color: Colors.orange,
                textColor: Colors.white,
                child: const Text('Contacts', style: TextStyle(fontSize: 30)),
              )),
          const SizedBox(height: 30),
          ButtonTheme(
              minWidth: 200.0,
              height: 100.0,
              child: RaisedButton(
                onPressed: () async {
                  await calendarAnalytics;
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Calendar(),
                          settings: RouteSettings(name: "CalendarView")));
                },
                color: Colors.orange,
                textColor: Colors.white,
                child: const Text('Calendar', style: TextStyle(fontSize: 30)),
              )),
          const SizedBox(height: 30),
          ButtonTheme(
              minWidth: 200.0,
              height: 100.0,
              child: RaisedButton(
                onPressed: () async {
                  await excersicesAnalytics;
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => VideoExerciseList(),
                          settings: RouteSettings(name: "ExcersisesView")));
                },
                color: Colors.orange,
                textColor: Colors.white,
                child: const Text('Exercises', style: TextStyle(fontSize: 30)),
              )),
          const SizedBox(height: 30),
          Container(
              height: 100,
              width: 100,
              child: FloatingActionButton(
                onPressed: () async {
                  await voiceAnalytics;
                  listening();
                },
                child: Icon(
                  Icons.mic_rounded,
                  color: Colors.white,
                  size: 50,
                ),
              ))
        ],
      ),
    );
  }
}
