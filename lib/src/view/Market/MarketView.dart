import 'package:flutter/material.dart';
import 'package:connectivity/connectivity.dart';
import 'package:jointdoctor_app/src/view/NavigationBar/NavigationBar.dart';

class MarketView extends StatefulWidget {
  @override
  _MarketViewState createState() => _MarketViewState();
}

class _MarketViewState extends State<MarketView> {
  List products = [];
  List productFiltered = [];
  Map<String, Color> contactsColorMap = new Map();
  TextEditingController searchController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    _checkConnectivity();
  }

  _showDialog(title, text) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(title),
            content: Text(text),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  _checkConnectivity() {
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        return Scaffold(
            appBar: AppBar(
              title: Text("Market", style: TextStyle(color: Colors.white)),
            ),
            body: Center(
                child: _showDialog("No internet connection",
                    "You are not connected to a network")));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    bool isSearching = searchController.text.isNotEmpty;
    return Scaffold(
        appBar: AppBar(
          title: Text("Market", style: TextStyle(color: Colors.white)),
          centerTitle: true,
        ),
        bottomNavigationBar: NavBar(index: 1),
        body: Builder(builder: (context) {
          if (products.isEmpty) {
            products.add("value");
            return Align(
              alignment: Alignment.bottomCenter,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          } else {
            return Container(
              padding: EdgeInsets.all(20),
              child: Column(
                children: <Widget>[
                  Container(
                    child: TextField(
                      controller: searchController,
                      decoration: InputDecoration(
                          labelText: 'Search',
                          border: new OutlineInputBorder(
                              borderSide: new BorderSide(
                                  color: Theme.of(context).primaryColor)),
                          prefixIcon: Icon(Icons.search,
                              color: Theme.of(context).primaryColor)),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: isSearching == true
                          ? productFiltered.length
                          : products.length,
                      itemBuilder: (context, index) {
                        var product = isSearching == true
                            ? productFiltered[index]
                            : products[index];
                        return ListTile(
                          title: Text("Nombre"),
                          subtitle: Text("Tipo"),
                        );
                      },
                    ),
                  )
                ],
              ),
            );
          }
        }));
  }
}
