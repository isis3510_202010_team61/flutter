import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:jointdoctor_app/src/controller/LogInState.dart';
import 'package:jointdoctor_app/src/model/DB.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:connectivity/connectivity.dart';

class Calendar extends StatefulWidget {
  @override
  _CalendarState createState() => _CalendarState();
}

class _CalendarState extends State<Calendar> {
  String eventName = "";
  DateTime date;
  bool isAllDay = false;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  List<Meeting> events = <Meeting>[];
  List<dynamic> appointments = <dynamic>[];

  @override
  void initState() {
    super.initState();
    getEvents();
  }

  _showDialog(title, text) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(title),
            content: Text(text),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  _checkConnectivity() {
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        return Scaffold(
            appBar: AppBar(
              title: Text("Contacts", style: TextStyle(color: Colors.white)),
            ),
            body: Center(
                child: _showDialog("No internet connection",
                    "You are not connected to a network")));
      }
    });
  }

  Future<void> showAddEventDialog(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (context) {
          final TextEditingController _eventName = TextEditingController();
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
              title: Text("AddEvent"),
              content: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      TextFormField(
                        controller: _eventName,
                        validator: (value) {
                          setState(() {
                            eventName = value;
                          });
                          return value.isNotEmpty ? null : "Invalid field";
                        },
                        decoration:
                            InputDecoration(hintText: "Enter event name"),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Is all day?"),
                          Checkbox(
                              value: isAllDay,
                              onChanged: (checked) {
                                setState(() {
                                  isAllDay = checked;
                                });
                              })
                        ],
                      )
                    ],
                  )),
              actions: <Widget>[
                TextButton(
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        Navigator.of(context).pop();
                      }
                      await addEvent();
                    },
                    child: Text("submit"))
              ],
            );
          });
        });
  }

  Future<void> addEvent() async {
    var user = Provider.of<LogInState>(context, listen: false).currentUser();
    await Provider.of<DB>(context, listen: false)
        .addCalendarEvent(user.uid, eventName, date, date, isAllDay);
  }

  Future<void> getEvents() async {
    var user = Provider.of<LogInState>(context, listen: false).currentUser();
    var e = await Provider.of<DB>(context, listen: false)
        .getCalendarEvents(user.uid);
    List eventList = [];
    e.docs.forEach((event) {
      print(event.data()["from"] is Timestamp);
      eventList.add(event.data());
    });
    eventList.forEach((event) {
      setState(() {
        if (event["from"] == null || event["to"] == null) {
        } else {
          events.add(Meeting(event["eventName"], event["from"].toDate(),
              event["to"].toDate(), Colors.blue, event["isAllDay"]));
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    _checkConnectivity;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Calendar",
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: Colors.orange,
      ),
      body: SfCalendar(
          view: CalendarView.month,
          dataSource: MeetingDataSource(events),
          monthViewSettings: MonthViewSettings(
              appointmentDisplayMode: MonthAppointmentDisplayMode.appointment,
              showAgenda: true),
          onTap: (CalendarTapDetails details) {
            print(details.appointments);
            setState(() {
              date = details.date;
              appointments = details.appointments;
            });
          },
          onLongPress: (CalendarLongPressDetails details) async {
            setState(() {
              date = details.date;
              appointments = details.appointments;
            });
            await showAddEventDialog(context);
          }),
    );
  }
}

class MeetingDataSource extends CalendarDataSource {
  MeetingDataSource(List<Meeting> source) {
    appointments = source;
  }

  @override
  DateTime getStartTime(int index) {
    return appointments[index].from;
  }

  @override
  DateTime getEndTime(int index) {
    return appointments[index].to;
  }

  @override
  String getSubject(int index) {
    return appointments[index].eventName;
  }

  @override
  Color getColor(int index) {
    return appointments[index].background;
  }

  @override
  bool isAllDay(int index) {
    return appointments[index].isAllDay;
  }
}

class Meeting {
  Meeting(this.eventName, this.from, this.to, this.background, this.isAllDay);

  String eventName;
  DateTime from;
  DateTime to;
  Color background;
  bool isAllDay;

  @override
  toString() => 'eventName: $eventName';
}
