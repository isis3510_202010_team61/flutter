import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LogInState extends ChangeNotifier{
  FirebaseAuth auth = FirebaseAuth.instance;

  bool loggedIn = false;
  bool loading = false;

  User _user;

  bool isLoggedIn() => loggedIn;
  bool isLoading() => loading;

  User currentUser() => _user;

  void logIn() async {
    loading = true;
    notifyListeners();
      var user = await signInWithGoogle();
      _user = user.user;
      loading = false;
      if(_user != null) {
        loggedIn = true;
        notifyListeners();
      } else {
        loggedIn = false;
        notifyListeners();
      }
  }

  void logOut(){
    loggedIn = false;
    GoogleSignIn().signOut();
    notifyListeners();
  }

  Future<UserCredential> signInWithGoogle() async {
    // Trigger the authentication flow
    final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication googleAuth = await googleUser.authentication;

    // Create a new credential
    final GoogleAuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithCredential(credential);
  }
}