# jointdoctor_app

A new Flutter application.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.io/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.io/docs/cookbook)

For help getting started with Flutter, view our 
[online documentation](https://flutter.io/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.


Steps to follow in order to execute the jointdoctor_app

1. Clone the project from the repository
2. Once the project is cloned Open it on Android Studio
3. Open the project and type in the terminal: flutter pub get .In order to instal the dependencies. The output must print a 0 as is indicates a succesfull download and update of dependencies.
4. Set the emulator to run on the 'main.dart' file.
5. Select a virtual device of your choice. (We mainly try with a connected device or the virtual Nexus5X API28X86).
6. Wait until the project has loaded all the necessary files.
7. Click con the green play button to execute the main.dart file and wait for the app to open.